from random import randint

print("Hi, what is your name?")
player_name = input("Name: ")

print("Thanks", player_name, ", it's nice to meet you!" )

for guess_number in range (1,6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Guess", guess_number, ":", player_name, ", were you born in",
        month_number, "/", year_number, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("Oh well, you win! I have more important things to do than play this silly game. Goodbye!")
    else:
        print("Drat! Lemme try again!")
